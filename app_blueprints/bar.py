from flask import Flask, Blueprint
from flask_restplus import Resource, Api

bp = Blueprint('foo', __name__)
api = Api(bp)

@api.route('/')
class BarResource(Resource):
    def get(self):
        return {'hello': 'bar'}
