venv: requirements.txt
	test -d venv || python3.7 -m venv venv
	. venv/bin/activate; pip3 install -Ur requirements.txt
	touch venv/bin/activate

build: venv

clean:
	rm -rf venv
	find -iname "*.pyc" -delete
	find -iname "*.swp" -delete
	find -iname "*.swo" -delete
