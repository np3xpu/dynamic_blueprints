from flask import Flask
from flask_restplus import Resource, Api
import importlib

app = Flask(__name__)
api = Api(app)

@api.route('/foo')
class FooResourceLoader(Resource):
    def get(self):
        register_blueprint('foo')
        return 'should get foo'

@api.route('/bar')
class BarResourceLoader(Resource):
    def get(self):
        register_blueprint('bar')
        return 'should get bar'

def register_blueprint(api):
    print('register')
    module = importlib.import_module(f'app_blueprints.{api}')
    blueprint = getattr(module, 'bp')
    app.register_blueprint(blueprint)

if __name__ == '__main__':
    app.run(debug=True)
